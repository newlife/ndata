package ndata

import (
	"container/list"
	//	"fmt"
	//"gitee.com/newlife/ndata/list"
	//"math"
	//	"runtime"
)

type Stack struct {
	List *list.List
}

type Queue struct {
	List *list.List
}

func (stack *Stack) pop() interface{} {
	if elem := stack.List.Back(); elem != nil {
		stack.List.Remove(elem)
		return elem.Value
	}
	return nil
}

func (stack *Stack) push(elem interface{}) {
	stack.List.PushBack(elem)
}

func (queue *Queue) pop() interface{} {
	if elem := queue.List.Front(); elem != nil {
		queue.List.Remove(elem)
		return elem.Value
	}
	return nil
}

func (queue *Queue) push(elem interface{}) {
	queue.List.PushBack(elem)
}

type tree struct {
	root   *node
	length int
}

type node struct {
	value int
	left  *node
	right *node
}

func (t *tree) insert(value int) {
	newNode := &node{value, nil, nil}
	if t.root == nil {
		t.root = newNode
		t.length = 1
	} else {
		var currentNode = t.root
		for currentNode != nil {
			if currentNode.value > newNode.value {
				if currentNode.left != nil {
					currentNode = currentNode.left
				} else {
					currentNode.left = newNode
					break
				}

			} else {
				if currentNode.right != nil {
					currentNode = currentNode.right
				} else {
					currentNode.right = newNode
					break
				}
			}
		}
		t.length = t.length + 1
	}

}

func (t *tree) preorderwalk() []int {
	var node_list []int
	stack := Stack{List: list.New()}
	stack.push(t.root)

	elem := stack.pop()
	for elem != nil {
		node, _ := elem.(*node)
		node_list = append(node_list, node.value)
		if right := node.right; right != nil {
			stack.push(right)
		}
		if left := node.left; left != nil {
			stack.push(left)
		}
		elem = stack.pop()
	}
	return node_list
}

func (t *tree) inorderwalk() []int {
	var node_list []int
	stack := new(Stack)
	current := t.root
	if stack.List.Len() > 0 || current != nil {
		if current != nil {
			stack.push(current)
			current = current.left
		} else {
			current := stack.pop().(*node)
			node_list = append(node_list, current.value)
			current = current.right
		}
	}
	return node_list
}

func (t *tree) postorder() []int {
	var node_list []int
	stack1 := Stack{List: list.New()}
	stack2 := Stack{List: list.New()}
	stack1.push(t.root)
	for stack1.List.Len() > 0 {
		current := stack1.pop().(*node)
		stack2.push(current)
		if current.left != nil {
			stack1.push(current.left)
		}
		if current.right != nil {
			stack1.push(current.right)
		}
	}
	for stack2.List.Len() > 0 {
		elem := stack2.pop().(*node)
		node_list = append(node_list, elem.value)
	}
	return node_list
}

func (t *tree) levelorder() []int {
	var node_list []int
	queue := Queue{List: list.New()}
	nlast := t.root
	queue.push(nlast)
	for queue.List.Len() > 0 {
		current := queue.pop().(*node)
		node_list = append(node_list, current.value)
		if current.left != nil {
			queue.push(current.left)
			nlast = current.left
		}
		if current.right != nil {
			queue.push(current.right)
			nlast = current.right
		}
		// if current == nlast && (current.left == nil || current.right == nil) {

		// }

	}

	return node_list
}
