package ndata

import (
	"gitee.com/newlife/ndata"
	"testing"
)

// func TestGBT_Insert(t *testing.T) {
// 	arr := ndata.RandomSlice(0, 20, 10)
// 	newtree := &tree{}
// 	for _, v := range arr {
// 		newtree.insert(v)
// 		t.Log("tree insert ", v)
// 		t.Log("tree length ", newtree.length)
// 	}
// }

func TestGBT_preorderwalk(t *testing.T) {
	arr := ndata.RandomSlice(0, 20, 10)
	newtree := &tree{}
	for _, v := range arr {
		newtree.insert(v)
		t.Log("tree insert ", v)
	}
	t.Log(newtree.preorderwalk())
}

func TestGBT_inorderwalk(t *testing.T) {
	arr := ndata.RandomSlice(0, 20, 10)
	newtree := &tree{}
	for _, v := range arr {
		newtree.insert(v)
		t.Log("tree insert ", v)
	}
	t.Log(newtree.preorderwalk())
}

func TestGBT_postorder(t *testing.T) {
	arr := ndata.RandomSlice(0, 20, 10)
	newtree := &tree{}
	for _, v := range arr {
		newtree.insert(v)
		t.Log("tree insert ", v)
	}
	t.Log(newtree.postorder())
}
func TestGBT_levelorder(t *testing.T) {
	arr := ndata.RandomSlice(0, 40, 7)
	newtree := &tree{}
	for _, v := range arr {
		newtree.insert(v)
		t.Log("tree insert ", v)
	}
	t.Log(newtree.levelorder())
}
